package io.scenarium.video;

import io.beanmanager.PluginsBeanSupplier;
import io.beanmanager.consumer.ClassNameRedirectionConsumer;
import io.scenarium.flow.consumer.OperatorConsumer;
import io.scenarium.flow.consumer.PluginsFlowSupplier;
import io.scenarium.pluginManager.PluginsSupplier;
import io.scenarium.video.operator.network.MjpegStreamer;
import io.scenarium.video.operator.recorder.Mp4Recorder;

public class Plugin implements PluginsSupplier, PluginsFlowSupplier, PluginsBeanSupplier {
	@Override
	public void populateOperators(OperatorConsumer operatorConsumer) {
		operatorConsumer.accept(Mp4Recorder.class);
		operatorConsumer.accept(MjpegStreamer.class);
	}

	@Override
	public void populateClassNameRedirection(ClassNameRedirectionConsumer classNameRedirectionConsumer) {
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.network.MjpegStreamer", "io.scenarium.video.operator.network.MjpegStreamer");
		classNameRedirectionConsumer.accept("io.scenarium.core.operator.network.recorder.Mp4Recorder", "io.scenarium.video.operator.recorder.Mp4Recorder");
	}

}
