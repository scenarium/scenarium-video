/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package io.scenarium.video.operator.recorder;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import io.beanmanager.editors.PropertyInfo;
import io.beanmanager.editors.basic.PathInfo;
import io.beanmanager.editors.primitive.number.ControlType;
import io.beanmanager.editors.primitive.number.NumberInfo;
import io.scenarium.core.editors.NotChangeableAtRuntime;
import io.scenarium.flow.EvolvedOperator;

import org.jcodec.api.awt.AWTSequenceEncoder;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.model.Rational;

public class Mp4Recorder extends EvolvedOperator {
	@PropertyInfo(index = 0, info = "Output mp4 file")
	@NotChangeableAtRuntime
	@PathInfo(directory = false, filters = "Mp4 mp4")
	private File outFile;
	@PropertyInfo(index = 1, info = "Frame rate of encoding video")
	@NumberInfo(min = 1, controlType = ControlType.SPINNER)
	@NotChangeableAtRuntime
	private int frameRate = 25;
	@PropertyInfo(index = 2, info = "Quantization parameter of the H264 video encoder. It's directly linked to the compression rate of the videos. Lower for better quality")
	@NumberInfo(min = 1, max = 30, controlType = ControlType.SPINNER)
	@NotChangeableAtRuntime
	private int qp = 25;
	private AWTSequenceEncoder enc;

	@Override
	public void birth() throws FileNotFoundException, IOException {
		if (this.outFile == null)
			setWarning("no mp4 output file");
		else {
			this.enc = null;
			if (!this.outFile.exists())
				this.outFile.createNewFile();
			this.enc = new AWTSequenceEncoder(NIOUtils.writableFileChannel(this.outFile.getAbsolutePath()), Rational.R(this.frameRate, 1), this.qp);
		}
	}

	@Override
	public void death() {
		if (this.enc != null) {
			try {
				this.enc.finish();
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.enc = null;
		}
	}

	public int getFrameRate() {
		return this.frameRate;
	}

	public File getOutFile() {
		return this.outFile;
	}

	public int getQp() {
		return this.qp;
	}

	public void process(BufferedImage img) {
		if (this.enc == null)
			return;
		if (img.getWidth() % 2 != 0 || img.getHeight() % 2 != 0) {
			BufferedImage bgrImage = new BufferedImage((int) Math.ceil(img.getWidth() / 2.0) * 2, (int) Math.ceil(img.getHeight() / 2.0) * 2, BufferedImage.TYPE_3BYTE_BGR);
			bgrImage.getGraphics().drawImage(img, 0, 0, null);
			img = bgrImage;
		}
		try {
			this.enc.encodeImage(img);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setFrameRate(int frameRate) {
		this.frameRate = frameRate;
	}

	public void setOutFile(File outFile) {
		this.outFile = outFile;
	}

	public void setQp(int qp) {
		this.qp = qp;
	}
}
