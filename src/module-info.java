import io.scenarium.pluginManager.PluginsSupplier;
import io.scenarium.video.Plugin;

module io.scenarium.video {
	uses PluginsSupplier;

	provides PluginsSupplier with Plugin;

	requires transitive io.scenarium.image;

	requires jdk.httpserver;
	requires Jcodec;

	exports io.scenarium.video;
	exports io.scenarium.video.operator.network;
	exports io.scenarium.video.operator.recorder;

}
